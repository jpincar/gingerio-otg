from django.shortcuts import get_object_or_404, render

from datetime import datetime, timedelta
from django.db.models import Case, Count, IntegerField, Sum, When
from django.utils import timezone

from .models import Event, Vendor

def index(request):
    return render(request, 'otgweb/index.html')

def events(request):
    events = Event.objects.order_by('-start_time')
    context = {'events': events}
    return render(request, 'otgweb/events.html', context)

def event_detail(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    context = {'event': event}
    return render(request, 'otgweb/event_detail.html', context)

def vendors(request):
    now = timezone.now()
    recent = now - timedelta(days=30)

    vendors = Vendor.objects.all().annotate(num_recent_events=Sum(
        Case(
            When(membership__event__start_time__lte=now, membership__event__start_time__gte=recent, then=1),
            default=0, output_field=IntegerField()
        )
    )).order_by('-num_recent_events')

    context = {'vendors': vendors}
    return render(request, 'otgweb/vendors.html', context)

def vendor_detail(request, vendor_id):
    vendor = get_object_or_404(Vendor, pk=vendor_id)

    now = timezone.now()
    recent = now - timedelta(days=30)
    num_recent_events = vendor.event_set.filter(membership__event__start_time__lte=now, membership__event__start_time__gte=recent).count()

    context = {'vendor': vendor, 'num_recent_events': num_recent_events}
    return render(request, 'otgweb/vendor_detail.html', context)
