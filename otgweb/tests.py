from django.test import TestCase

from django.utils import timezone
from django.urls import reverse

from otgweb.models import Event, Vendor, Membership

import datetime

def create_event(name, days_after_now, fb_graph_id, otg_event_id):
    now = timezone.now()
    description = "Test description"
    place_name = "Test place name"
    start_time = now + datetime.timedelta(days=days_after_now)
    end_time = start_time + datetime.timedelta(hours=3)
    return Event.objects.create(name=name, description=description, place_name=place_name, start_time=start_time, end_time=end_time, fb_graph_id=fb_graph_id, otg_event_id=otg_event_id)

def create_vendor(name, otg_vendor_id):
    website = "Test website"
    cuisine = "Test cuisine"
    logo_url = "http://logos.test/logo.png"
    return Vendor.objects.create(name=name, website=website, cuisine=cuisine, logo_url=logo_url, otg_vendor_id=otg_vendor_id)

def create_membership(event, vendor):
    return Membership.objects.create(event=event, vendor=vendor)

class IndexViewTests(TestCase):
    def test_index_view(self):
        response = self.client.get(reverse('otgweb:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Index")
        self.assertContains(response, "Events")
        self.assertContains(response, "Vendors")

class EventIndexViewTests(TestCase):
    def test_event_index_view(self):
        response = self.client.get(reverse('otgweb:events'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['events'], [])
        self.assertContains(response, "No events are available.")

    def test_event_index_view_with_data(self):
        event01 = create_event("Minna", 3, '1234', '987')
        event02 = create_event("Bryant", -3, '2345', '876')
        response = self.client.get(reverse('otgweb:events'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['events'], [repr(event01), repr(event02)])
        self.assertContains(response, event01.name)
        self.assertContains(response, event02.name)

class EventDetailViewTests(TestCase):
    def setUp(self):
        self.event = create_event("Minna", 3, '1234', '987')

    def test_event_detail_view(self):
        response = self.client.get(reverse('otgweb:event_detail', args=(self.event.id,)))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['event'], self.event)
        self.assertContains(response, self.event.name)

    def test_event_detail_view_with_data(self):
        vendor = create_vendor("Taco guy", '444')
        create_membership(self.event, vendor)
        response = self.client.get(reverse('otgweb:event_detail', args=(self.event.id,)))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['event'], self.event)
        self.assertContains(response, self.event.name)
        self.assertContains(response, vendor.name)

class VendorIndexViewTests(TestCase):
    def test_vendor_index_view(self):
        response = self.client.get(reverse('otgweb:vendors'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['vendors'], [])
        self.assertContains(response, "No vendors are available.")

    def test_vendor_index_view_with_data(self):
        vendor01 = create_vendor("Taco guy", "444")
        vendor02 = create_vendor("Sushi guy", "555")
        event01 = create_event("Minna", -5, '1234', '987')
        event02 = create_event("Bryant", -12, '2345', '876')

        # Create memberships to test sort order
        # vendor01 has more events so it should be first
        create_membership(event01, vendor01)
        create_membership(event01, vendor02)
        create_membership(event02, vendor01)

        response = self.client.get(reverse('otgweb:vendors'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['vendors'], [repr(vendor01), repr(vendor02)])
        self.assertContains(response, vendor01.name)
        self.assertContains(response, vendor02.name)

    def test_vendor_index_view_with_future_events(self):
        vendor01 = create_vendor("Taco guy", "444")
        vendor02 = create_vendor("Sushi guy", "555")
        event01 = create_event("Minna", -5, '1234', '987')
        event_future_01 = create_event("Howard", 3, '3456', '765')
        event_future_02 = create_event("Townsend", 6, '4567', '654')

        # Create memberships to test sort order
        # vendor02 has more events, but they're in the future, so vendor01 should come first
        create_membership(event01, vendor01)
        create_membership(event_future_01, vendor02)
        create_membership(event_future_01, vendor02)

        response = self.client.get(reverse('otgweb:vendors'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['vendors'], [repr(vendor01), repr(vendor02)])
        self.assertContains(response, vendor01.name)
        self.assertContains(response, vendor02.name)

class VendorDetailViewTests(TestCase):
    def setUp(self):
        self.vendor = create_vendor("Taco guy", "444")
        self.event01 = create_event("Minna", 3, '1234', '987')
        create_membership(self.event01, self.vendor)

    def test_vendor_detail_view(self):
        response = self.client.get(reverse('otgweb:vendor_detail', args=(self.vendor.id,)))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['vendor'], self.vendor)
        self.assertContains(response, self.vendor.name)
        self.assertContains(response, self.event01.name)
