from django.contrib import admin


from .models import Event, Vendor, Membership

admin.site.register(Event)
admin.site.register(Vendor)
admin.site.register(Membership)
