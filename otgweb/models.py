from __future__ import unicode_literals

from django.db import models

class Event(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    start_time = models.DateTimeField()
    place_name = models.CharField(max_length=255)
    end_time = models.DateTimeField()
    fb_graph_id = models.CharField(max_length=255)
    otg_event_id = models.IntegerField(null=True, blank=True)
    vendors = models.ManyToManyField('Vendor', through='Membership')

    def __str__(self):
        return self.name

class Vendor(models.Model):
    name = models.CharField(max_length=255)
    website = models.CharField(max_length=255, null=True, blank=True)
    cuisine = models.CharField(max_length=255, null=True, blank=True)
    logo_url = models.CharField(max_length=255, null=True, blank=True)
    otg_vendor_id = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Membership(models.Model):
    event = models.ForeignKey('Event')
    vendor = models.ForeignKey('Vendor')

    def __str__(self):
        return "Event({}), Vendor({})".format(self.event, self.vendor)
