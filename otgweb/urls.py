from django.conf.urls import url

from . import views

app_name = "otgweb"
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^events/$', views.events, name='events'),
    url(r'^events/(?P<event_id>[0-9]+)/$', views.event_detail, name='event_detail'),
    url(r'^vendors/$', views.vendors, name='vendors'),
    url(r'^vendors/(?P<vendor_id>[0-9]+)/$', views.vendor_detail, name='vendor_detail'),
]
