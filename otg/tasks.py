from __future__ import absolute_import

from otg.celery import app

import dateutil.parser
import json
import re
import requests
import urllib

FACEBOOK_APP_ID     = '1170309713007815'
FACEBOOK_APP_SECRET = '301061550331c9fcb9f4340cb3f98fd7'

@app.task
def fetch_otg_data():
    # Need to import in here since django isn't loaded initially
    from otgweb.models import Event, Vendor, Membership

    oauth_args = dict(client_id = FACEBOOK_APP_ID, client_secret = FACEBOOK_APP_SECRET, grant_type = 'client_credentials')
    url = 'https://graph.facebook.com/oauth/access_token?' + urllib.urlencode(oauth_args)
    response = requests.get(url)
    token = response.text.split("=")[1]
    url = "https://graph.facebook.com/v2.7/OffTheGridSF/events?access_token=" + token
    response = requests.get(url)
    data = json.loads(response.text)["data"]
    for event_data in data:
        fb_graph_id = event_data["id"]
        name = event_data["name"]
        description = event_data["description"]
        start_time = dateutil.parser.parse(event_data["start_time"])
        end_time = dateutil.parser.parse(event_data["end_time"])
        place = event_data["place"]["name"]

        event = Event.objects.filter(fb_graph_id=fb_graph_id).first()
        if not event:
            event = Event(name=name, description=description, start_time=start_time, end_time=end_time, place_name=place, fb_graph_id=fb_graph_id)
            event.save()

            matches = re.search('https://offthegrid\.com/event/(\d+)\/', description)
            if matches:
                otg_event_id = matches.group(1)

                url = "https://offthegrid.com/otg-api/passthrough/markets/{}.json/".format(otg_event_id)

                # Default user-agent returns a 403 for some reason
                headers = {'user-agent': 'curl/7.43.0'}

                response = requests.get(url, headers=headers)
                data = json.loads(response.text)
                market_detail = data['MarketDetail']
                events = market_detail['Events']

                start_time = event.start_time
                target_date = '{0:0>2}.{1:0>2}'.format(start_time.month, start_time.day)

                event_data = next((event for event in events if event['Event']['month_day'] == target_date), None)
                if event_data:
                    existing_vendors = event.vendors.all()
                    vendors = event_data['Vendors']
                    for vendor_data in vendors:
                        otg_vendor_id = vendor_data['id']
                        name = vendor_data['name']
                        website = vendor_data['website']
                        cuisine = vendor_data['cuisine']
                        logo_url = vendor_data['logo_url']
                        vendor = Vendor.objects.filter(otg_vendor_id=otg_vendor_id).first()
                        if not vendor:
                            vendor = Vendor(name=name, website=website, cuisine=cuisine, logo_url=logo_url, otg_vendor_id=otg_vendor_id)
                            vendor.save()
                        if vendor not in existing_vendors:
                            Membership.objects.create(event_id=event.id, vendor_id=vendor.id)
